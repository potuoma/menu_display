# WEB CHANNEL
## Running the webchannel locally


## setup
- install nodejs(https://nodejs.org/en/download/package-manager/)  
- install npm  (https://docs.npmjs.com/getting-started/installing-node)  
- [Set up ssh keys for your computer](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html)
- clone the repository  
`git clone git@bitbucket.org:interintel/wc_dev_env.git`


```
    git submodule init;
    git submodule update;
    npm install;
    
```

- Configure a service payload in relation to `db.structure.js`  
- Configure the service name in `config.js`

- Install bower components
```
cd polymer;
bower install
```
- run 
`npm start;`